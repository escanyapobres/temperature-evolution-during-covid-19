# Temperature evolution during COVID-19
## Brief description
_English:_ Code that I made during my COVID-19 contagion to practise Python plotting. This is just a plot about the evolution of my temperature.

_Valencià/català:_ Codi que he fet durant el meu contagi per COVID-19 per practicar el graficat amb Python. Només és una gràfica sobre l'evolució de la meua temperatura.

![Temperatura](imgs/virus.png)

