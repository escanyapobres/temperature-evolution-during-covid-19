# Càrrega de les llibreries
from scipy.interpolate import interp1d
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

# Dies des del contagi
x = np.array([5+9/24, 6+9/24, 7+9/24, 8+9/24, 9+9/24, 10+9/24, 10+18/24, 10+20/24, 10+20/22, 11, 11+10/24, 11+15/24, 11+17/24, 11+19/24, 12+10/24, 12+12/24, 13+22/24, 14+9/24, 15+9/24, 16+9/24])

# Temperatura
y = np.array([36.3, 36.4, 36.5, 36.3, 36.2, 36.3, 36.8, 37.1, 37.3, 36.6, 36.6, 37.1, 36.9, 36.8, 36.4, 36.6, 36.3, 36.4, 36.4, 36.2])

# Interpolació
f = interp1d(x, y, kind='nearest')
interpol = np.linspace(5+9/24, 16+9/24, num=1000, endpoint=True)

# Gràfic
figure(num=None, figsize=(12.5, 6.5), dpi=100, facecolor='w', edgecolor='k')
plt.xticks(np.arange(5, 17+1, 1.0))
plt.plot(x, y, 'o', interpol, f(interpol), '-r')
plt.xlabel("Temps des del contagi (dies)")
plt.ylabel("Temperatura (ºC)")
plt.title('Variació de la meua temperatura corporal durant l\'infecció per COVID-19')
plt.legend(['Temperatura (ºC)', 'Interpolació'], loc='best')
plt.grid()
# Temperatura normal
plt.text(13.8, 36.45, 'Temperatura normal')
plt.axhline(y=36.4, color='k', linestyle='dashed', linewidth=1)

# PCR positiva
plt.text(6.7, 37.1, 'PCR positiva')
plt.axvline(6.5, color='k', linestyle='dashed', linewidth=1)

# Alta
plt.text(15.3, 37.1, 'Alta')
plt.axvline(16, color='k', linestyle='dashed', linewidth=1)

# Guardem i mostrem la gràfica
plt.savefig('virus.png')
plt.show()